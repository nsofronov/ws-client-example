# Web services client generation example

## Web-services and their description

SOAP (originally Simple Object Access Protocol) is a messaging protocol specification for exchanging structured information in the implementation of web services in computer networks.

The **W**eb **S**ervices **D**escription **L**anguage is an XML-based interface definition language that is used for describing the functionality offered by a web service. 

Security Master has SOAP API and the service are described by WSDL.

It is possible to convert WSDL to Java classes

### Importing current project

You can clone the repository from 

```
git clone https://nsofronov@bitbucket.org/nsofronov/ws-client-example.git
```

And command **service:runService** is running a simple SOAP service (**gradlew.bat** is file from current repository)

```
gradlew.bat service:runService
```

You can see wsdl from the service on [http://localhost:9999/examplews?wsdl](http://localhost:9999/examplews?wsdl)

## Generating java classes from WSDL

### Instruments

1. [Apache CXF](http://cxf.apache.org/)

1. [Wsimport (JDK tool)](https://docs.oracle.com/javase/6/docs/technotes/tools/share/wsimport.html)

1. [Soap UI](https://www.soapui.org/)

1. [IntelliJ IDEA generating instruments](https://www.jetbrains.com/help/idea/generate-java-code-from-wsdl-or-wadl-dialog.html)

1. [Eclipse CXF plugin](http://help.eclipse.org/kepler/index.jsp?topic=%2Forg.eclipse.jst.ws.cxf.doc.user%2Ftasks%2Fcreate_client.html)

1. [Eclipse Web Services plugin](https://help.eclipse.org/neon/index.jsp?topic=%2Forg.eclipse.jst.ws.axis2.ui.doc.user%2Ftopics%2Ftaxis2td.html)

etc.

### An example with Apache CXF

Run wsdl2java.bat with wsdl location parameter

```
wsdl2java.bat http://localhost:9999/examplews?wsdl
```

There are source files in directory **com**

You should copy their to your project.

### An example with Wsimport

Run wsimport command from jdk with wsdl location parameter

```
wsimport -keep -verbose http://localhost:9999/examplews?wsdl
```

There are source files in directory **com**

You should copy their to your project.

## Building simple WS client

When you append new files to you classpath you'll be able to create connection to the service

Simple client code

```java
DefaultWebServiceImplService service = new DefaultWebServiceImplService();
DefaultWebService webService = service.getDefaultWebServiceImplPort();
String text = webService.getHelloWorldAsString("Nikolay");
System.out.println(text);
```

## Problems with generating SecMaster API

## Work with object with the same names

Security master api has different parameters with the same names and there is a special property to work with objects with the same names

```
-autoNameResoulution
```

Command example

```
wsdl2java.bat -autoNameResoulution http://localhost:9999/examplews?wsdl
```

## Soap header supporting

Security master api uses SOAP header, but our service implementation doesn't support their.

By default proxy web service is **WSBindingProvider** and it support soap headers

Usage in code (When proxy service is built)

```java
GlobalAPISoap apiSoap = getGlobalAPISoap();
JAXBElement<GlobalUserInfoHeader> header = new ObjectFactory().createGlobalUserInfoHeader(globalUserInfoHeader);
((WSBindingProvider) apiSoap).setOutboundHeaders(header);
return apiSoap;
```

There is WSBindingProvider in JDK or **jaxws-rt** library

Gradle dependency example
```
compile group: 'com.sun.xml.ws', name: 'jaxws-rt', version: '2.3.0'
```