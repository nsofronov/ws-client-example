package com.broadridge.wsclientexample.client;

import com.broadridge.wsclientexample.service.DefaultWebServiceImplService;
import com.broadridge.wsclientexample.webservice.DefaultWebService;

public class DefaultWebServiceClient {
    public static void main(String[] args) {
        DefaultWebServiceImplService service = new DefaultWebServiceImplService();
        DefaultWebService webService = service.getDefaultWebServiceImplPort();
        String text = webService.getHelloWorldAsString("Nikolay");
        System.out.println(text);
    }
}
