package com.broadridge.wsclientexample.service;

import javax.xml.ws.Endpoint;
import javax.xml.ws.handler.Handler;
import java.util.List;

public class DefaultWebServicePublisher {
    public static void main(String[] args) {
        Endpoint ep = Endpoint.create(new DefaultWebServiceImpl());
        List<Handler> handlerChain = ep.getBinding().getHandlerChain();
        handlerChain.add(new SOAPLoggingHandler());
        ep.getBinding().setHandlerChain(handlerChain);
        ep.publish("http://localhost:9999/examplews");
    }
}
