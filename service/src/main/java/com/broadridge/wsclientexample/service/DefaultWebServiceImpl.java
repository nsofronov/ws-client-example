package com.broadridge.wsclientexample.service;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService(endpointInterface = "com.broadridge.wsclientexample.service.DefaultWebService")
public class DefaultWebServiceImpl implements DefaultWebService {

    @Override
    @WebMethod
    public String getHelloWorldAsString(String name) {
        return "Hello World JAX-WS " + name;
    }
}
