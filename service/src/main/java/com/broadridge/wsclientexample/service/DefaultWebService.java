package com.broadridge.wsclientexample.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(targetNamespace = "http://webservice.wsclientexample.broadridge.com/", name = "DefaultWebService")
public interface DefaultWebService {
    @WebMethod
    public String getHelloWorldAsString(@WebParam(name = "name", targetNamespace = "") String name);
}
